jQuery(function($){
	"use strict";



var CONVOCATION = window.CONVOCATION || {};




/* ==================================================
	Contact Form Validations
================================================== */
//	CONVOCATION.RegistrationForm = function(){
//		$('.registration-form').each(function(){
//			var formInstance = $(this);
//			formInstance.submit(function(){
//		
//			var action = $(this).attr('action');
//		
//			$("#message").slideUp(750,function() {
//			$('#message').hide();
//		
//			$('#submit')
//				.after('<img src="images/assets/ajax-loader.gif" class="loader" />')
//				.attr('disabled','disabled');
//		
//			$.post(action, {
//				plan: $('input[name=plan]:checked').val(),
//				name: $('#Name').val(),
//				email: $('#Email').val(),
//				phone: $('#Phone').val()
//			},
//				function(data){
//					document.getElementById('message').innerHTML = data;
//					$('#message').slideDown('slow');
//					$('.registration-form img.loader').fadeOut('slow',function(){$(this).remove()});
//					$('#submit').removeAttr('disabled');
//				}
//			);
//			});
//			return false;
//		});
//		});
//	}
/* ==================================================
	contact Us Form Validations
================================================== */
	CONVOCATION.contactUsForm = function(){
		$('.contactus-form').each(function(){
			var formInstance = $(this);
			formInstance.submit(function(){

			var action = $(this).attr('action');
		
			$("#contactus_message").slideUp(750,function() {
			$('#contactus_message').hide();
		
			$('#contactus_submit')
				.after('<img src="images/assets/ajax-loader.gif" class="loader" />')
				.attr('disabled','disabled');

			$.post(action, {
				first: $('#contactus_first').val(),
				mail: $('#contactus_mail').val(),
				last: $('#contactus_last').val(),
				text: $('#contactus_text').val(),
				'g-recaptcha-response': $('#g-recaptcha-response').val(),
			}//,
//				function(data){
//					document.getElementById('contactus_message').innerHTML = data;
//					$('#contactus_message').slideDown('slow');
//					$('.contactus-form img.loader').fadeOut('slow',function(){$(this).remove()});
//					$('#contactus_submit').removeAttr('disabled');
//                                        grecaptcha.reset(widgetId1);
//				}
			);
			});
			return false;
		});
		});
	}
/* ==================================================
	Slack Form Validations
================================================== */
	CONVOCATION.SlackForm = function(){
		$('.slack-form').each(function(){
			var formInstance = $(this);
			formInstance.submit(function(){

			var action = $(this).attr('action');
		
			$("#slack_message").slideUp(750,function() {
				$('#slack_message').hide();
		
				$('#slack_submit')
					.after('<img src="images/assets/ajax-loader.gif" class="loader" />')
					.attr('disabled','disabled');

				$.post(action, {
						first: $('#slack_first').val(),
						mail: $('#slack_mail').val(),
						last: $('#slack_last').val(),
						'g-recaptcha-response': $('#g-recaptcha-response').val(),
					},
					function(data){
						document.getElementById('slack_message').innerHTML = data;
						$('#slack_message').slideDown('slow');
						$('.slack-form img.loader').fadeOut('slow',function(){$(this).remove()});
						$('#slack_submit').removeAttr('disabled');
	                                        grecaptcha.reset();
					}
				);
			});
			return false;
		});
		});
	}
/* ==================================================
	Payment Form stuff
================================================== */
    $("#checkoutModal").on('hidden.bs.modal', function () {
        // reset the form now
        $(".clearme").val("");
        var $form = $('#payment-form');
        $form.find('.submit').prop('disabled', false); // Re-enable submission
        $("#payment_message").slideUp(250,function() {
            $('#payment_message').hide();
            $('.payment-form img.loader').fadeOut('fast',function(){$(this).remove()}); 
        });
    });
    // allow a dropdown to appear when selected to contribute another amount 
    $('#sponsor_level').on('change', function() {
        var $form = $('#payment-form');
        $form.find('#checkout').prop('disabled', false); // Re-enable submission
        // set grand_total to 1000 + donation if it's shown
//        if ( this.value == "Sponsor the Community" ) {
//            if ($('.donation').is(':visible')) {
//                $form.find('#grand_total').val(+'1000' + +$('#donation').val()) 
//            } else {
//                $form.find('#grand_total').val(+'1000') 
//            }
//
//        }
        // set grand_total to 150 + donation if it's shown
//        if (this.value == "Sponsor a Meeting"){
//            if ($('.donation').is(':visible')) {
//                $form.find('#grand_total').val(+'150' + +$('#donation').val()) 
//            } else {
//                $form.find('#grand_total').val(+'150') 
//            }
//
//        }

        // set grand_total to 300 + donation if it's shown
        if (this.value == "Sponsor SecDSM"){
            if ($('.donation').is(':visible')) {
                $form.find('#grand_total').val(+'300' + +$('#donation').val()) 
            } else {
                $form.find('#grand_total').val(+'300') 
            }

        }

        // if "Contribute Another Amount" is selected thenshow it it
        if ( this.value == 'Contribute Another Amount')  {
            $('.another_amount').slideDown('fast');
            $(".another_amount").show();
//            if ($('.donation').is(':visible')) {
                $form.find('#grand_total').val(+$('#another_amount').val() + +$('#donation').val()) 
//            } else {
//                $form.find('#grand_total').val(+$('#contribute_another_amount'))
//            }

        } 
        // if anything other than "Contribute Another Amount" is selected then hide it
        if ( this.value != 'Contribute Another Amount')  {
            $('.another_amount').slideUp(250,function() {
                $(".another_amount").hide();
            });
        }
    });

    // update grand total when donation get updated
    $('#donation').bind('input', function() {
        var $form = $('#payment-form');
//        if ( $('#sponsor_level').val() == "Sponsor the Community" ) {
//            $form.find('#grand_total').val(+'1000' + +$('#donation').val());
//        }
        if ( $('#sponsor_level').val() == "Sponsor SecDSM" ) {
            $form.find('#grand_total').val(+'300' + +$('#donation').val());
        }
//        if ( $('#sponsor_level').val() == "Sponsor a Meeting"){
//            $form.find('#grand_total').val(+'150' + +$('#donation').val());
//        }
        if ( $('#sponsor_level').val() == 'Contribute Another Amount') {
            $form.find('#grand_total').val(+$('#another_amount').val() + +$('#donation').val());
        }
    }); 

    // update grand total when contribute another amount gets updated
    $('#another_amount').bind('input', function() {
        var $form = $('#payment-form');
        if ( $('#sponsor_level').val() == 'Contribute Another Amount') {
            $form.find('#grand_total').val(+$('#another_amount').val() + +$('#donation').val());
        }
    }); 

    // add a click event for the add a donation button 
    $("#add_a_donation").click(function() {
      var $form = $('#payment-form');
      // if it's visible, hide it and reset it
      if ($('.donation').is(':visible')) {
          $("#donation").val('0');
          // update grand_total
          if ( $('#sponsor_level').val() == "Sponsor SecDSM" ) {
              $form.find('#grand_total').val(+'300');
          }
//          if ( $('#sponsor_level').val() == "Sponsor the Community" ) {
//              $form.find('#grand_total').val(+'1000');
//          }
//          if ( $('#sponsor_level').val() == "Sponsor a Meeting"){
//              $form.find('#grand_total').val(+'150');
//          }
          if ( $('#sponsor_level').val() == 'Contribute Another Amount') {
              $form.find('#grand_total').val(+$('#another_amount').val());
          }
          $(".donation").slideUp(250,function() {
              $(".donation").hide();
          });
      // otherwise show it
      } else {
        $('.donation').slideDown('fast');
        $(".donation").show();
      }
    });


    $(function() {
      var $form = $('#payment-form');
      $form.submit(function(event) {
        // Disable the submit button to prevent repeated clicks:
        $form.find('#payment_submit').prop('disabled', true);
        $("#payment_message").slideUp(250,function() {
            $('#payment_message').hide();
            $('#payment_submit').after('<img src="../images/assets/ajax-loader.gif" class="loader" />')
        });
        // Request a token from Stripe:
        Stripe.card.createToken($form, stripeResponseHandler);
    
        // Prevent the form from being submitted:
        return false;
      });
    });
      

    //response handler for stripe 
    function stripeResponseHandler(status, response) {
        // Grab the form:
        var $form = $('#payment-form');
        
        if (response.error) { // Problem!
        
          // Show the errors on the form:
         
          $(".clearme").val("");
          $form.find('#payment_submit').prop('disabled', false); // Re-enable submission
          document.getElementById('payment_message').innerHTML = '<div class=\'alert alert-error\'><h3 class=\'short\'>' + response.error.message +'</h3></div>';
          $('#payment_message').slideDown('fast');
          $('.payment-form img.loader').fadeOut('fast',function(){$(this).remove()});
        
        } else { // Token was created!
        
          // Get the token ID:
          var token = response.id;
        
          // Insert the token ID into the form so it gets submitted to the server:
          if ( $('#form_name').val() == 'merchandise') {
              $.post('../api/payment.php', {
                  'name':   $('#name').val(),
                  'email':   $('#email').val(),
                  'stripeToken':  token,
                  'city':    $('#city').val(),
                  'address': $('#address').val(),
                  'state':   $('#state').val(),
                  'zip':     $('#zip').val(),
                  'tshirt_size':  $('#tshirt_size').val(),
                  'tshirt_q':     $('#tshirt_q').val(),
                  'polo_size':    $('#polo_size').val(),
                  'polo_q':       $('#polo_q').val(),
                  'stocking_q':   $('#stocking_q').val(),
                  'grand_total':  $('#grand_total').val(),
                  'donation':     $('#donation').val(),
                  'form_name':    $('#form_name').val(),
                  'delivery':     $('#delivery').val(),
              },
              function(data){
                  document.getElementById('payment_message').innerHTML = data;
                  $('#payment_message').slideDown('fast');
                  $('.payment-form img.loader').fadeOut('fast',function(){$(this).remove()});
                  $(".clearme").val("");
                  $form.find('#payment_submit').prop('disabled', false);
              });
           } else if ( $('#form_name').val() == 'sponsorship') {
              $.post('../api/payment.php', {
                  'name':          $('#name').val(),
                  'email':          $('#email').val(),
                  'stripeToken':    token,
                  'city':           $('#city').val(),
                  'address':        $('#address').val(),
                  'state':          $('#state').val(),
                  'zip':            $('#zip').val(),
                  'sponsor_name':   $('#sponsor_name').val(),
                  'form_name':      $('#form_name').val(),
                  'sponsor_level':  $('#sponsor_level').val(), 
                  'sponsor_month':  $('#sponsor_month').val(),
                  'sponsor_year' :  $('#sponsor_year').val(),
                  'another_amount': $('#another_amount').val(),
                  'grand_total':    $('#grand_total').val(),
                  'donation':       $('#donation').val(),
              },
              function(data){
                  document.getElementById('payment_message').innerHTML = data;
                  $('#payment_message').slideDown('fast');
                  $('.payment-form img.loader').fadeOut('fast',function(){$(this).remove()});
                  $(".clearme").val("");
                  $form.find('#payment_submit').prop('disabled', false);
              });
           }
        }
    };

/* ==================================================
/* ==================================================
	Scroll to Top
================================================== */
//	CONVOCATION.scrollToTop = function(){
//		var windowWidth = $(window).width(),
//			didScroll = false;
//	
//		var $arrow = $('#back-to-top');
//		var $sharefloat = $('.social-share');
//	
//		$arrow.click(function(e) {
//			$('body,html').animate({ scrollTop: "0" }, 750, 'easeOutExpo' );
//			e.preventDefault();
//		})
//	
//		$(window).scroll(function() {
//			didScroll = true;
//		});
//	
//		setInterval(function() {
//			if( didScroll ) {
//				didScroll = false;
//	
//				if( $(window).scrollTop() > 200 ) {
//					$arrow.css("right",10);
//					$sharefloat.css("right",60);
//				} else {
//					$arrow.css("right","-40px");
//					$sharefloat.css("right",10);
//				}
//			}
//		}, 250);
//	}
/* ==================================================
   Accordion
================================================== */
	CONVOCATION.accordion = function(){
		var accordion_trigger = $('.accordion-heading.accordionize');
		
		accordion_trigger.delegate('.accordion-toggle','click', function(event){
			if($(this).hasClass('active')){
				$(this).removeClass('active');
				$(this).addClass('inactive');
			}
			else{
				accordion_trigger.find('.active').addClass('inactive');          
				accordion_trigger.find('.active').removeClass('active');   
				$(this).removeClass('inactive');
				$(this).addClass('active');
			}
			event.preventDefault();
		});
	}
/* ==================================================
   Toggle
================================================== */
	CONVOCATION.toggle = function(){
		var accordion_trigger_toggle = $('.accordion-heading.togglize');
		
		accordion_trigger_toggle.delegate('.accordion-toggle','click', function(event){
			if($(this).hasClass('active')){
				$(this).removeClass('active');
				$(this).addClass('inactive');
			}
			else{
				$(this).removeClass('inactive');
				$(this).addClass('active');
			}
			event.preventDefault();
		});
	}
/* ==================================================
   Tooltip
================================================== */
//	CONVOCATION.toolTip = function(){ 
//		$('a[data-toggle=tooltip]').tooltip();
//		$('a[data-toggle=popover]').popover({html:true}).click(function(e) { 
//       e.preventDefault(); 
//       $(this).focus(); 
//   });
//	}
/* ==================================================
   Twitter Widget
================================================== */
//	CONVOCATION.TwitterWidget = function() {
//		$('.twitter-widget').each(function(){
//			var twitterInstance = $(this); 
//			var twitterTweets = twitterInstance.attr("data-tweets-count") ? twitterInstance.attr("data-tweets-count") : "1"
//			twitterInstance.twittie({
//          	dateFormat: '%b. %d, %Y',
//            	template: '<li><i class="fa fa-twitter"></i> {{tweet}} <span class="date">{{date}}</span></li>',
//            	count: twitterTweets,
//            	hideReplies: true
//        	});
//		});
//	}
/* ==================================================
   Hero Flex Slider
================================================== */
	CONVOCATION.heroflex = function() {
		$('.flexslider').each(function(){
				var carouselInstance = $(this); 
				var carouselAutoplay = carouselInstance.attr("data-autoplay") == 'yes' ? true : false
				var carouselPagination = carouselInstance.attr("data-pagination") == 'yes' ? true : false
				var carouselArrows = carouselInstance.attr("data-arrows") == 'yes' ? true : false
				var carouselDirection = carouselInstance.attr("data-direction") ? carouselInstance.attr("data-direction") : "horizontal"
				var carouselStyle = carouselInstance.attr("data-style") ? carouselInstance.attr("data-style") : "fade"
				var carouselSpeed = carouselInstance.attr("data-speed") ? carouselInstance.attr("data-speed") : "5000"
				var carouselPause = carouselInstance.attr("data-pause") == 'yes' ? true : false
				
				carouselInstance.flexslider({
					animation: carouselStyle,
					easing: "swing",               
					direction: carouselDirection,       
					slideshow: carouselAutoplay,              
					slideshowSpeed: carouselSpeed,         
					animationSpeed: 600,         
					initDelay: 0,              
					randomize: false,            
					pauseOnHover: carouselPause,       
					controlNav: carouselPagination,           
					directionNav: carouselArrows,            
					prevText: "",          
					nextText: "",
					start: function () {
					  $('.flex-caption').show();
					  BackgroundCheck.init({
						targets: '.body',
						images: '.flexslider li.parallax'
					  });
					},
					after: function () {
					  BackgroundCheck.refresh();
					  $('.flex-caption').show();
					}
				});
		});
	}
/* ==================================================
   PrettyPhoto
================================================== */
//	CONVOCATION.PrettyPhoto = function() {
//		$("a[data-rel^='prettyPhoto']").prettyPhoto({
//			  opacity: 0.5,
//			  social_tools: "",
//			  deeplinking: false
//		});
//	}
/* ==================================================
   Animated Counters
================================================== */
//	CONVOCATION.Counters = function() {
//		$('.counters').each(function () {
//			$(".timer .count").appear(function() {
//			var counter = $(this).html();
//			$(this).countTo({
//				from: 0,
//				to: counter,
//				speed: 2000,
//				refreshInterval: 60,
//				});
//			});
//		});
//	}
/* ==================================================
   SuperFish menu
================================================== */
//	CONVOCATION.SuperFish = function() {
//		$('.sf-menu').superfish({
//			  delay: 200,
//			  animation: {opacity:'show', height:'show'},
//			  speed: 'fast',
//			  cssArrows: false,
//			  disableHI: true
//		});
//		$(".main-navigation > ul > li > ul > li:has(ul)").find("a:first").append(" <i class='fa fa-angle-right'></i>");
//		$(".main-navigation > ul > li > ul > li > ul > li:has(ul)").find("a:first").append(" <i class='fa fa-angle-right'></i>");
//	}
/* ==================================================
   Header Functions
================================================== */
	CONVOCATION.StickyHeader = function() {
		var windowWidth = $(window).width(),
		didScroll = false;
	
		var $menu = $('.site-header');

	        var $fileName = location.href.split("/").slice(-1);


		$(window).scroll(function() {
			didScroll = true;
		});
	
		setInterval(function() {
	
				if ( $fileName.indexOf("archives.html") == -1 ) {
				    if( $(window).scrollTop() > 20 ) {
                                        $menu.addClass('sticky-header');
                                    } else {
                                        $menu.removeClass('sticky-header');
                                    }
                                }
		}, 250);
		setInterval(function() {
	
				if ( $fileName.indexOf("archives.html") == -1 ) {
                                    if( $(window).scrollTop() > 120 ) {
					$menu.addClass('tranlucent');
                                    } else {
					$menu.removeClass('tranlucent');
				    }
                                }
		}, 250);
	}
/* ==================================================
	Responsive Nav Menu
================================================== */
	CONVOCATION.MobileMenu = function() {
                $(".sf-menu > li >a").click(function(){
                        if($(window).width() < 992){
                            $(this).toggleClass("opened");
                            $(".main-navigation").slideToggle();
//                          return false;
                        }
                });
		// Responsive Menu Events
		$('#menu-toggle').click(function(){
			$(this).toggleClass("opened");
			$(".main-navigation").slideToggle();
			return false;
		});
		if($(window).width() < 992){
			var WHG = $(window).height();
			var HH = $('.site-header').height();
			var WNH = WHG -HH;
			$('.main-navigation').css("height",WNH);
		}
		$(window).resize(function(){
			if($(window).width() > 992) {
				$(".main-navigation").css("display","block");
				} else {
				$(".main-navigation").css("display","none");
			}
		});
		$(window).resize(function(){
			if($("#menu-toggle").hasClass("opened") && ($(window).width() < 992)) {
				$(".main-navigation").css("display","block");
			} else if (!$("#menu-toggle").hasClass("opened") && ($(window).width() < 992)) {
				$(".main-navigation").css("display","none");
			}
		});
	}
/* ==================================================
   Flickr Widget
================================================== */
//	CONVOCATION.FlickrWidget = function() {
//		$('.flickr-widget').each(function(){
//			var flickrInstance = $(this); 
//			var flickrImages = flickrInstance.attr("data-images-count") ? flickrInstance.attr("data-images-count") : "1"
//			var flickrUserid = flickrInstance.attr("data-flickr-userid") ? flickrInstance.attr("data-flickr-userid") : "1"
//			flickrInstance.jflickrfeed({
//				limit: flickrImages,
//				qstrings: {
//					id: flickrUserid
//				},
//				itemTemplate: '<li><a href="{{image_b}}"><img alt="{{title}}" src="{{image_s}}" /></a></li>'
//			});
//		});
//	}
/* ==================================================
   Init Functions
================================================== */
$(document).ready(function(){
//	CONVOCATION.RegistrationForm();
	CONVOCATION.SlackForm();
//        CONVOCATION.contactUsForm();
//	CONVOCATION.scrollToTop();
	CONVOCATION.accordion();
//	CONVOCATION.toggle();
//	CONVOCATION.toolTip();
//	CONVOCATION.TwitterWidget();
//	CONVOCATION.PrettyPhoto();
//	CONVOCATION.SuperFish();
//	CONVOCATION.Counters();
	CONVOCATION.StickyHeader();
	CONVOCATION.MobileMenu();
//	CONVOCATION.heroflex();
//	CONVOCATION.FlickrWidget();
});

/* get the forms to fill out themseves */
$(document).ready(function() {
	$('input').change(function() {
		$('[name="' + $(this).attr('name') + '"]').val($(this).val());
	});
});

// Pages Design Functions

// Any Button Scroll to section
//$('.scrollto').click(function(){
//	$.scrollTo( this.hash, 800, { easing:'easeOutQuint' });
//	return false;
//});

// FITVIDS
//$(".fw-video").fitVids();

// Image Hover icons for gallery items
//var MBC = function(){
//	$(".media-box .zoom").each(function(){
//		mpwidth = $(this).parent().width();
//		mpheight = $(this).parent().find("img").height();
//	
//		$(this).css("width", mpwidth);
//		$(this).css("height", mpheight);
//		$(this).css("line-height", mpheight+"px");
//	});
//}
$(document).ready(function(){
	$(".format-image").each(function(){
		$(this).find(".media-box").append("<span class='zoom'><span class='icon'><i class='icon-image'></i></span></span>");
	});
	$(".format-standard").each(function(){
		$(this).find(".media-box").append("<span class='zoom'><span class='icon'><i class='icon-eye'></i></span></span>");
	});
	$(".format-video").each(function(){
		$(this).find(".media-box").append("<span class='zoom'><span class='icon'><i class='icon-music-play'></i></span></span>");
	});
	$(".format-link").each(function(){
		$(this).find(".media-box").append("<span class='zoom'><span class='icon'><i class='fa fa-link'></i></span></span>");
	});
//	$('.pricing-plans input[name=plan]:checked').parent('.plan-option').addClass("selected");
//        $('.pricing-plans input[name=plan]').click(function () {
//            $('.pricing-plans input[name=plan]:not(:checked)').parent('.plan-option').removeClass("selected");
//            $('.pricing-plans input[name=plan]:checked').parent('.plan-option').addClass("selected");
//      });
//	$(".plan-option").each(function(){
//			if($(this).hasClass('selected')){
//				$(this).append("<span class='plan-selection'><span class='btn btn-default btn-transparent'><i class='fa fa-check'></i> Selected</span></span>");
//			} else {
//				$(this).append("<span class='plan-selection'><span class='btn btn-default btn-transparent'>Select</span></span>");
//			}
//	});
//	$(".plan-option").click(function(){
//		$(".plan-option").find(".plan-selection").find(".btn").html('Select');
//		$(this).find(".plan-selection").find(".btn").html('<i class="fa fa-check"></i> Selected');
//	});
//	if(Modernizr.touch && $(window).width() < 991 ) {
//		$(".sf-menu > li > a").click(function(e){
//			$(".main-navigation").slideUp();
//			e.preventDefault();
//		});
//	}
//	$('.share-float').click(function(e){
//		e.preventDefault();
//	});
//	$('.social-share').hover(function(e){
//		if($(this).hasClass('opened')){
//			$('.social-share .social-icons').delay(1000).animate({height:'0',opacity:0}, "fast", "easeInQuad");
//			$(this).removeClass('opened');
//			$(this).addClass('closed');
//		} else {
//			$('.social-share .social-icons').animate({height:'160px',opacity:1}, "fast", "easeOutQuad");
//			$('.social-share').css("overflow","visible");
//			$(this).removeClass('closed');
//			$(this).addClass('opened');
//		}
//	});
	CONVOCATION.StickyHeader();
});


$(document).ready(function(){
	// Icon Append
	$('.basic-link').append(' <i class="fa fa-angle-right"></i>');
	$('.basic-link.backward').prepend(' <i class="fa fa-angle-left"></i> ');
	$(".nav-tabs li").prepend('<i class="fa fa-caret-down"></i> ');
	$('ul.checks li').prepend('<i class="fa fa-check"></i> ');
	$('ul.angles li, .nav-list-primary li a > a:first-child').prepend('<i class="fa fa-angle-right"></i> ');
	$('ul.inline li').prepend('<i class="fa fa-check-circle-o"></i> ');
	$('ul.chevrons li').prepend('<i class="fa fa-chevron-right"></i> ');
	$('ul.carets li').prepend('<i class="fa fa-caret-right"></i> ');
	$('a.external').prepend('<i class="fa fa-external-link"></i> ');
	// Centering the dropdown menus
	$(".main-navigation ul li").mouseover(function() {
		 var the_width = $(this).find("a").width();
		 var child_width = $(this).find("ul").width();
		 var width = parseInt((child_width - the_width)/2);
		 $(this).find("ul").css('left', -width);
	});
	var $tallestCol;
	$('.pricing-plans').each(function(){
	   $tallestCol = 0;
	   $(this).find('.inclusive').
		each(function(){
			($(this).height() > $tallestCol) ? $tallestCol = $(this).height() : $tallestCol = $tallestCol;
		});   
		if($tallestCol == 0) $tallestCol = 'auto';
		$(".inclusive ul").css('height',$tallestCol);
	});
});

// Animation Appear
$("[data-appear-animation]").each(function() {
	var $this = $(this);
	$this.addClass("appear-animation");
	if(!$("html").hasClass("no-csstransitions") && $(window).width() > 767) {
		$this.appear(function() {
			var delay = ($this.attr("data-appear-animation-delay") ? $this.attr("data-appear-animation-delay") : 1);
			if(delay > 1) $this.css("animation-delay", delay + "ms");
			$this.addClass($this.attr("data-appear-animation"));
			setTimeout(function() {
				$this.addClass("appear-animation-visible");
			}, delay);
		}, {accX: 0, accY: -150});
	} else {
		$this.addClass("appear-animation-visible");
	}
});
// Animation Progress Bars
$("[data-appear-progress-animation]").each(function() {
	var $this = $(this);
	$this.appear(function() {
		var delay = ($this.attr("data-appear-animation-delay") ? $this.attr("data-appear-animation-delay") : 1);
		if(delay > 1) $this.css("animation-delay", delay + "ms");
		$this.addClass($this.attr("data-appear-animation"));
		setTimeout(function() {
			$this.animate({
				width: $this.attr("data-appear-progress-animation")
			}, 1500, "easeOutQuad", function() {
				$this.find(".progress-bar-tooltip").animate({
					opacity: 1
				}, 500, "easeOutQuad");
			});
		}, delay);
	}, {accX: 0, accY: -50});
});

$(document).ready(function(){
	// Parallax Jquery Callings
//	if(!Modernizr.touch) {
//		$(window).bind('load', function () {
//			parallaxInit();						  
//		});
//	}
//	function parallaxInit() {
//		$('.parallax1').parallax("50%", 0.1);
//		$('.parallax2').parallax("50%", 0.1);
//		$('.parallax3').parallax("50%", 0.1);
//		$('.parallax4').parallax("50%", 0.1);
//		$('.parallax5').parallax("50%", 0.1);
//		$('.parallax6').parallax("50%", 0.1);
//		$('.parallax7').parallax("50%", 0.1);
//		$('.parallax8').parallax("50%", 0.1);
//		/*add as necessary*/
//	}
	
	//LOCAL SCROLL
//	jQuery('.sf-menu').localScroll({
//		offset: -62
//	});
	
//	var sections = jQuery('section');
//	var navigation_links = jQuery('.sf-menu a');
//	sections.waypoint({
//		handler: function(direction) {
//			var active_section;
//			active_section = jQuery(this);
//			if (direction === "up") active_section = active_section.prev();
//			var active_link = jQuery('.sf-menu a[href="#' + active_section.attr("id") + '"]');
//			navigation_links.removeClass("current");
//			active_link.addClass("current").delay(1500);
//		},
//		offset: 150
//	});
	// Window height/Width Getter Classes
	var wheighter = $(window).height();
	var wwidth = $(window).width();
	var wheightlh = wheighter - 80;
	$(".wheighter").css("height",wheighter);
	$(".wwidth").css("width",wwidth);
	$(".wheighterlh").css("height",wheightlh);
});
$(window).resize(function(){
	var wheighter = $(window).height();
	var wwidth = $(window).width();
	var wheightlh = wheighter - 80;
	$(".wheighter").css("height",wheighter);
	$(".wwidth").css("width",wwidth);
	$(".wheighterlh").css("height",wheightlh);
});
$(window).load(function(){
	$('.event-hero-info').fadeIn();
});
});